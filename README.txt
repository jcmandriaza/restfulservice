This is the implementation of a restfull JAX-

In order to compile an execute the application please run the following command in the 
root directory:

mvn spring-boot:run

After start-up, the interaction with the application happens through a REST client,
using the requests exemplified bellow:

http://localhost:8080/products

The used content type is JSON and the table bellow details the available API :

                        +--------+-------------------------------------------------------------+-----------------------------------------------------------------+
                        | VERB   | REQUEST                                                     |  RESPONSE                                                               |
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/products              | POST   | { "name" : "p1", "description" : "p1 description"}          | { "id": 10, "name": "p1", "description": "p1 description" }     |
+-----------------------+--------+------------------------------------------------------------ +-----------------------------------------------------------------+
|/products              | GET    |                                                             | [{"id":10,"name":"p1","description":"p1 description"}]          |
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/products/{id}         | POST   | { "name" : "p1", "description" : "p1 extended description"} | [{"id":10,"name":"p1","description":"p1 extended description"}] |
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/products/{id}         | GET    |                                                             | [{"id":10,"name":"p1","description":"p1 extended description"}] |
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/products/{id}         | DELETE |                                                             |                                                                 |
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/products/{id}/products| POST   | { "name" : "p1-d1", "description" : "p1 d1 description"}    | {"id": 11, "name": "p1-d1", "description": "p1 d1 description"} |
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/products/{id}/products| GET    |                                                             | [{"id":11,"name":"p1-d1","description":"p1 d1 description"}]    |
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/products/{id}/images  | POST   |                                                             | {"id": 20}
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/products/{id}/images  | GET    |                                                             | [{"id":20}]
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/detailed-products     | GET    |                                                             | [{"id":10,"name":"p1","description":"p1 extended description",
|                       |        |                                                             | "products":[{"id":11,"name":"p1-d1","description":"p1 d1 description",
|                       |        |                                                             | "products":[],"images":[]}],"images":[{"id":20}]},{"id":11,"name":"p1-d1",
|                       |        |                                                             |"description":"p1 d1 description","products":[],"images":[]}]
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/detailed-products/{id}| GET    |                                                             | {"id":10,"name":"p1","description":"p1 extended description",
|                       |        |                                                             | "products":[{"id":11,"name":"p1-d1","description":"p1 d1 description",
|                       |        |                                                             | "products":[],"images":[]}],"images":[{"id":20}]}
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+
|/products              | GET    |                                                             | [{"id":10,"name":"p1","description":"p1 extended description"},
|                       |        |                                                             | {"id":11,"name":"p1-d1","description":"p1 d1 description"}]
+-----------------------+--------+-------------------------------------------------------------+-----------------------------------------------------------------+	

In order to execute the tests please run the following command in the root directory:

mvn test

The tests does not provide proper coverage but they show the main principles are there. 

I believe the implementation properly covers the proposed specification and provides the simplest code base possible. 

Jaime Mandriaza
 