package jcmm.restfullservice.domain;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import jcmm.restfullservice.domain.Image;
import jcmm.restfullservice.domain.ImageRepository;
import jcmm.restfullservice.domain.Product;
import jcmm.restfullservice.domain.ProductRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JpaDataTest {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	ImageRepository imageRepository;

	Product product = new Product();
	Product parentProduct = new Product();
	Image image = new Image();

	@Before
	public void setUp() {
		parentProduct.setName("parent");
		parentProduct.setDescription("parent product description");

		parentProduct = productRepository.save(parentProduct);

		product.setParent(parentProduct);
		product.setName("child");
		product.setDescription("child description");

		product = productRepository.save(product);
		
		image.setProduct(parentProduct);
		
		image = imageRepository.save(image);
	}

	@Test
	public void testFindOneProduct() {
		assertThat(productRepository.findOne(parentProduct.getId()), both(notNullValue()).and(equalTo(parentProduct)));
	}

	@Test
	public void testFindOneImage() {
		assertThat(imageRepository.findOne(image.getId()), both(notNullValue()).and(equalTo(image)));
	}

	@Test
	public void testFindAllProducts() {
		assertThat(productRepository.findAll(), hasItems(parentProduct, product));
	}
}
