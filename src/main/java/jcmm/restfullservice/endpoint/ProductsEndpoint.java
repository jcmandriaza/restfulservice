package jcmm.restfullservice.endpoint;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonView;

import jcmm.restfullservice.domain.Image;
import jcmm.restfullservice.domain.ImageRepository;
import jcmm.restfullservice.domain.Product;
import jcmm.restfullservice.domain.ProductRepository;
import jcmm.restfullservice.domain.View;

@Component
@Path("/products")
public class ProductsEndpoint {

	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ImageRepository imageRepository;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Summary.class)
	public Iterable<Product> findAll() {
		return productRepository.findAll();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Summary.class)
	public Product addParentProduct(Product product) {
		return productRepository.save(product);
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Summary.class)
	public Product findOne(@PathParam("id") Long id) {
		return productRepository.findOne(id);
	}

	@POST
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Summary.class)
	public Product updateOne(@PathParam("id") Long id, Product product) {
		Product oldProduct = productRepository.findOne(id);
		if (oldProduct != null) {
			product.setId(oldProduct.getId());
		}
		return productRepository.save(product);
	}

	@DELETE
	@Path("/{id}")
	public void delete(@PathParam("id") Long id) {
		productRepository.delete(id);
	}

	@GET
	@Path("/{id}/images")
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Summary.class)
	public Iterable<Image> getImages(@PathParam("id") Long id) {
		Product product = productRepository.findOne(id);
		if (product == null) {
			return null;
		}
		return product.getImages();
	}

	@POST
	@Path("/{id}/images")
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Summary.class)
	public Image addImage(@PathParam("id") Long id) {
		Product product = productRepository.findOne(id);
		if (product == null) {
			return null;
		}
		Image image = new Image();
		image.setProduct(product);
		return imageRepository.save(image);
	}

	@GET
	@Path("/{id}/products")
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Summary.class)
	public Iterable<Product> getProducts(@PathParam("id") Long id) {
		Product products = productRepository.findOne(id);
		if (products == null) {
			return null;
		} 
		return products.getProducts();
	}

	@POST
	@Path("/{id}/products")
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Summary.class)
	public Product addProduct(@PathParam("id") Long id, Product product) {
		Product parent = productRepository.findOne(id);
		if (parent == null) {
			return null;
		} 
		product.setParent(parent);
		return productRepository.save(product);
	}
}
