package jcmm.restfullservice.endpoint;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonView;

import jcmm.restfullservice.domain.Product;
import jcmm.restfullservice.domain.ProductRepository;
import jcmm.restfullservice.domain.View;

@Component
@Path("/detailed-products")
public class DetailedProductsEndpoint {

	@Autowired
	private ProductRepository productRepository;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Detailed.class)
	public Iterable<Product> findAllDetailed() {
		return productRepository.findAll();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@JsonView(View.Detailed.class)
	public Product findOne(@PathParam("id") Long id) {
		return productRepository.findOne(id);
	}
}
