package jcmm.restfullservice;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import jcmm.restfullservice.endpoint.DetailedProductsEndpoint;
import jcmm.restfullservice.endpoint.ProductsEndpoint;

@Component
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(ProductsEndpoint.class);
		register(DetailedProductsEndpoint.class);
		register(CorsFilter.class);
	}
}
