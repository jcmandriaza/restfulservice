package jcmm.restfullservice.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class Image implements Serializable {

	private static final long serialVersionUID = -5269166912947957358L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@JsonView(View.Summary.class)
	private Long id;

	@ManyToOne
	private Product product;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (!(obj instanceof Image)) {
			return false;
		}
		Image other = (Image) obj;
		return this.getId().equals(other.getId());
	}
}
